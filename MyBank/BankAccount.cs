﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MyBank
{
    /*
      The core asset class
    */
    /// <summary>
    /// The core asset class.
    /// </summary>
    /// <remarks>
    /// Two features: Thread safe and high performance:
    /// 1. Using ReaderWriterLockSlim is based on the fact that the read operation is
    ///    far more than the write operation in terms of bank account.
    /// 2. Fine granularity - Execute the rw-lock operation only at that very moment
    ///    when the read/write operations really happen.
    /// </remarks>
    public class BankAccount
    {
        private ReaderWriterLockSlim rwLock = new ReaderWriterLockSlim();

        private double amount;
        public int Id { get; set; }
        public string Name { get; set; }

        public double Amount
        {
            get
            {
                rwLock.EnterReadLock();
                try
                {
                    return this.amount;
                }
                finally
                {
                    rwLock.ExitReadLock();
                }
            }
        }

        public void SetAmount(double transaction)
        {
            rwLock.EnterWriteLock();
            try
            {
                this.amount += transaction;
            }
            finally
            {
                rwLock.ExitWriteLock();
            }
        }

        public BankAccount(int id, string name, double amount)
        {
            this.Id = id;
            this.Name = name;
            this.amount = amount;
        }

        ~BankAccount()
        {
            if (rwLock != null)
            {
                rwLock.Dispose();
            }
        }
    }
}