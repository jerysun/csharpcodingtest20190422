﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBank
{
    /*
      An executing class doing the query
    */
    /// <summary>
    /// An executing class doing the query
    /// </summary>
    /// <remarks>
    /// Adopt the async programming for a potentially slow and unreliable remote network
    /// </remarks>
    public class AccountProcessing : IAccountService
    {
        public const string AccountIdIsNotFound = "AccountId is not found";
        public const string AccountsIsNull = "accounts is null";

        public IList<BankAccount> Accounts { get; set; }

        public AccountProcessing(IList<BankAccount> accounts)
        {
            this.Accounts = accounts ?? throw new NullReferenceException(AccountsIsNull);
        }

        public async Task<double> GetAccountAmount(int accountId)
        {
            double ret = await Task.Run(() =>
            {
                var ac = from ba in Accounts
                         where ba.Id == accountId
                         select ba;

                if (ac.ToList().Count == 0)
                {
                    throw new IdNotFoundException(AccountIdIsNotFound);
                }
                else
                {
                    foreach (var a in ac)
                    {
                        return a.Amount;
                    }
                }
                return 0.0;
            });

            return ret;
        }
    }
}
