﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBank
{
    /*
      A main operation class employing the DI design pattern
    */
    /// <summary>
    /// A main operation class employing the DI design pattern
    /// </summary>
    /// <remarks>
    /// Add the range check, null reference check; Adopt the async programming
    /// </remarks>
    public class AccountInfo
    {
        private readonly int _accountId;
        private readonly IAccountService _accountService;
        public const string AccountIdLessThanOrEqualZero = "AccountId is less than or equal to zero";
        public const string AccountServiceIsNull = "accountService is null";

        public AccountInfo(int accountId, IAccountService accountService)
        {
            if (accountId <= 0)
            {
                throw new ArgumentOutOfRangeException("accountId", accountId, AccountIdLessThanOrEqualZero);
            }
            else
            {
                _accountId = accountId;
            }

            _accountService = accountService ?? throw new NullReferenceException(AccountServiceIsNull);
        }

        public double Amount { get; private set; }

        public async Task RefreshAmount()
        {
            Amount = await _accountService.GetAccountAmount(_accountId);
        }
    }

}
