# CSharp Coding Test 2019-04-22

## I. Tasks

### PROBLEM 1
Given the following interface:

```
public interface IAccountService
{
    double GetAccountAmount(int accountId);
}
...and the following class that depends on this interface:

public class AccountInfo
{
	private readonly int _accountId;
	private readonly IAccountService _accountService;
	
	public AccountInfo(int accountId, IAccountService accountService)
	{
		_accountId = accountId;
		_accountService = accountService;
	}
	
	public double Amount { get; private set; }
	
	public void RefreshAmount()
	{
		Amount = _accountService.GetAccountAmount(_accountId);
	}
}
```
REQUIRED: Write a unit test that asserts the behaviour of RefreshAmount() method.

### PROBLEM 2
It has been determined that IAccountService.GetAccountAmount() is a potentially slow and
unreliable remote network call and that it should be made asynchronous.

Note that AccountInfo.RefreshAmount() may be invoked multiple times concurrently. Adjust
IAccountService and / or AccountInfo and your tests accordingly.

## II. Features

The solution MyBank solves the TWO problems mentioned above:

* Built on Visual Studio 2017 with the .Net Framework 4.5.2
* Completed all the required functionality
* Unit tests
* Thread safe
* Asynchronous programming

## III. Run

* Build the solution which includes two projects: MyBank and MyBankTests.
* Open the TestExplorer then click "Run All".

Please feel free to ask me if you have any questions.

Have fun,

Jerry Sun

Email:    jerysun007@hotmail.com