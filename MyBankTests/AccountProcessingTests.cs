﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyBank;

namespace MyBankTests
{
    /*
       The main unit test class
    */
    /// <summary>
    /// The main unit test class.
    /// </summary>
    /// <remarks>
    /// Assume the AccountAmount may be negative, which many people including myself have experienced in the real world.
    /// </remarks>

    [TestClass]
    public class AccountProcessingTests
    {
        private IList<BankAccount> accounts = new List<BankAccount>() {
            new BankAccount(1, "Alex", 392.50),
            new BankAccount(2, "Alisa", 192.50),
            new BankAccount(3, "Baldwin", 1392.57),
            new BankAccount(4, "Danielle", 22.50),
            new BankAccount(5, "Palma", 23920.99)
        };

        [TestMethod]
        public void Accounts_WhenNull_ThrowNullReferenceException()
        {
            IAccountService accountService = null;
            IList<BankAccount> accounts = null;

            try
            {
                accountService = new AccountProcessing(accounts);
            }
            catch (NullReferenceException e)
            {
                StringAssert.Contains(e.Message, AccountProcessing.AccountsIsNull);
                return;
            }
            Assert.Fail("No expected exception was thrown.");
        }

        [TestMethod]
        public async Task RefreshAmount_WhenIdDoesNotExist_ThrowIdNotIsFoundException()
        {
            int accountId = 991;
            IAccountService accountService = new AccountProcessing(accounts);
            AccountInfo accountInfo = new AccountInfo(accountId, accountService);

            try
            {
                await accountInfo.RefreshAmount();
            }
            catch (IdNotFoundException e)
            {
                StringAssert.Contains(e.Message, AccountProcessing.AccountIdIsNotFound);
                return;
            }
            Assert.Fail("No expected exception was thrown.");
        }
        
        [TestMethod]
        public async Task RefreshAmount_WithValidAccountId()
        {
            int accountId = 2;
            double expected = 192.50;
            double delta = 0.001;

            IAccountService accountService = new AccountProcessing(accounts);
            AccountInfo accountInfo = new AccountInfo(accountId, accountService);

            await accountInfo.RefreshAmount();
            double actual = accountInfo.Amount;
            Assert.AreEqual(expected, actual, delta, "Account Amout is not correct");
        }

    }
}