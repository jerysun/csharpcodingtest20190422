﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyBank;

namespace MyBankTests
{
    /*
      AccountInfo unit test
    */
    /// <summary>
    /// AccountInfo unit test
    /// </summary>
    /// <remarks>
    /// Check if it can deal with the situation that the AccountId is less than or equal 0,
    /// or AccountService instance is null when constructing the AccountInfo object.
    /// </remarks>
    [TestClass]
    public class AccountInfoTests
    {
        private IList<BankAccount> accounts = new List<BankAccount>() {
            new BankAccount(1, "Alex", 392.50),
            new BankAccount(2, "Alisa", 192.50),
            new BankAccount(3, "Baldwin", 1392.57),
            new BankAccount(4, "Danielle", 22.50),
            new BankAccount(5, "Palma", 23920.99)
        };

        [TestMethod]
        public void AccountId_WhenLessThanOrEqualZero_ShouldThrowArgumentOutOfRange()
        {
            int accountId = -2;
            IAccountService accountService = new AccountProcessing(accounts);
            AccountInfo accountInfo = null;

            try
            {
                accountInfo = new AccountInfo(accountId, accountService);
            }
            catch (ArgumentOutOfRangeException e)
            {
                StringAssert.Contains(e.Message, AccountInfo.AccountIdLessThanOrEqualZero);
                return;
            }
            Assert.Fail("No expected exception was thrown.");
        }

        [TestMethod]
        public void AccountService_WhenNull_ThrowNullReferenceException()
        {
            int accountId = 1;
            IAccountService accountService = null;
            AccountInfo accountInfo;

            try
            {
                accountInfo = new AccountInfo(accountId, accountService);
            }
            catch (NullReferenceException e)
            {
                StringAssert.Contains(e.Message, AccountInfo.AccountServiceIsNull);
                return;
            }
            Assert.Fail("No expected exception was thrown.");
        }
    }
}
